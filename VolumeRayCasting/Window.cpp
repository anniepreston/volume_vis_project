#include <QtGui>

#include "Window.h"
#include "GLWidget.h"
#include "TFWidget.h"

Window::Window(GLWidget* glContext, GradientWidget* tf, QMainWindow *parent):
    QMainWindow(parent),
    glWidget(glContext),
    tfWidget(tf)
{
    setCentralWidget(glWidget);

    setWindowTitle(tr("volume rendering"));

    QVBoxLayout *layout = new QVBoxLayout;
    glWidget->setLayout(layout);

    QDockWidget *dockWidget = new QDockWidget(tr("Transfer Function"), this);
    dockWidget->setWidget(tfWidget);
    addDockWidget(Qt::RightDockWidgetArea, dockWidget);

    createActions();
    createMenus();
}

void Window::createActions()
{
    bonsaiAct = new QAction(tr("&bonsai"), glWidget);
    engineAct = new QAction(tr("&engine"), glWidget);
    skullAct = new QAction(tr("&skull"), glWidget);

    bonsaiTFAct = new QAction(tr("&green-red"), glWidget);
    engineTFAct = new QAction(tr("&blue-purple"), glWidget);
    skullTFAct = new QAction(tr("&gray-white"), glWidget);

    whiteAct = new QAction(tr("&white"), glWidget);
    grayAct = new QAction(tr("&gray"), glWidget);
    blackAct = new QAction(tr("&black"), glWidget);

    bbAct = new QAction(tr("&bounding box"), glWidget);
    nobbAct = new QAction(tr("&no box"), glWidget);

    QSignalMapper* signalMapper = new QSignalMapper (this);
    QSignalMapper* tf_signalMapper = new QSignalMapper (this);
    QSignalMapper* bg_signalMapper = new QSignalMapper (this);
    QSignalMapper* bb_signalMapper = new QSignalMapper (this);

    connect(bonsaiAct, SIGNAL(triggered()), signalMapper, SLOT(map()));
    connect(engineAct, SIGNAL(triggered()), signalMapper, SLOT(map()));
    connect(skullAct, SIGNAL(triggered()), signalMapper, SLOT(map()));

    connect(bonsaiTFAct, SIGNAL(triggered()), tf_signalMapper, SLOT(map()));
    connect(engineTFAct, SIGNAL(triggered()), tf_signalMapper, SLOT(map()));
    connect(skullTFAct, SIGNAL(triggered()), tf_signalMapper, SLOT(map()));

    connect(whiteAct, SIGNAL(triggered()), bg_signalMapper, SLOT(map()));
    connect(grayAct, SIGNAL(triggered()), bg_signalMapper, SLOT(map()));
    connect(blackAct, SIGNAL(triggered()), bg_signalMapper, SLOT(map()));

    connect(bbAct, SIGNAL(triggered()), bb_signalMapper, SLOT(map()));
    connect(nobbAct, SIGNAL(triggered()), bb_signalMapper, SLOT(map()));

    QString bonsai = "bonsai.raw";
    QString engine = "Engine.raw";
    QString skull = "skull.raw";

    QString bonsai_tf = "tf_bonsai.txt";
    QString engine_tf = "tf_engine.txt";
    QString skull_tf = "tf_skull.txt";

    QString white = "white";
    QString gray = "gray";
    QString black = "black";

    QString bb = "true";
    QString nobb = "false";

    signalMapper -> setMapping (bonsaiAct, bonsai);
    signalMapper -> setMapping (engineAct, engine);
    signalMapper -> setMapping (skullAct, skull);

    tf_signalMapper -> setMapping (bonsaiTFAct, bonsai_tf);
    tf_signalMapper -> setMapping (engineTFAct, engine_tf);
    tf_signalMapper -> setMapping (skullTFAct, skull_tf);

    bg_signalMapper -> setMapping (whiteAct, white);
    bg_signalMapper -> setMapping (grayAct, gray);
    bg_signalMapper -> setMapping (blackAct, black);

    bb_signalMapper -> setMapping (bbAct, bb);
    bb_signalMapper -> setMapping (nobbAct, nobb);

    connect(signalMapper, SIGNAL(mapped(QString)), glWidget, SLOT(updateFile(QString)));
    connect(tf_signalMapper, SIGNAL(mapped(QString)), glWidget, SLOT(updateTF(QString)));
    connect(bg_signalMapper, SIGNAL(mapped(QString)), glWidget, SLOT(updateBG(QString)));
    connect(bb_signalMapper, SIGNAL(mapped(QString)), glWidget, SLOT(updateBB(QString)));
}

void Window::createMenus()
{
    //data file menu
    datasetMenu = menuBar()->addMenu(tr("&Dataset"));
    datasetMenu->addAction(bonsaiAct);
    datasetMenu->addAction(engineAct);
    datasetMenu->addAction(skullAct);

    //transfer function menu
    tfMenu = menuBar()->addMenu(tr("&Colors"));
    tfMenu->addAction(bonsaiTFAct);
    tfMenu->addAction(engineTFAct);
    tfMenu->addAction(skullTFAct);

    //bg color menu
    bgMenu = menuBar()->addMenu(tr("&Background"));
    bgMenu->addAction(whiteAct);
    bgMenu->addAction(grayAct);
    bgMenu->addAction(blackAct);

    //bb menu
    bbMenu = menuBar()->addMenu(tr("&View"));
    bbMenu->addAction(bbAct);
    bbMenu->addAction(nobbAct);
}
