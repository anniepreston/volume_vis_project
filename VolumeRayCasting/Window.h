#ifndef WINDOW_H
#define WINDOW_H

#include <QWidget>
#include <QMainWindow>
#include "GLWidget.h"
#include "TFWidget.h"
#include <string>

class QAction;
class QLabel;
class QMenu;

//widget boss, including menus

class Window : public QMainWindow
{
    Q_OBJECT

public:
    Window(GLWidget* glContext, GradientWidget* tf, QMainWindow *parent = 0);

protected:

public slots:

signals:
    void mapped(QString);

private:
    void createMenus();
    void createActions();

    GLWidget* glWidget;
    //ShadeWidget* tfWidget;
    GradientWidget* tfWidget;

    QMenu *datasetMenu;
    QMenu *tfMenu;
    QMenu *bgMenu;
    QMenu *bbMenu;

    QAction *bonsaiAct;
    QAction *engineAct;
    QAction *skullAct;

    QAction *bonsaiTFAct;
    QAction *engineTFAct;
    QAction *skullTFAct;

    QAction *whiteAct;
    QAction *grayAct;
    QAction *blackAct;

    QAction *bbAct;
    QAction *nobbAct;

};

#endif // WINDOW_H
