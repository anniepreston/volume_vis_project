#version 330

in vec3 CameraDir;
in vec3 CameraPos;
out vec4 FragColor;

uniform bool bbtest;

uniform vec3 volumeScale;
uniform mat4 invModelView;
uniform sampler3D volumeTex;
uniform sampler2D transferFuncTex;

//testing preintegration...
uniform sampler2D preintTex;

//lighting info
vec3 LightColor = vec3 (0.5, 0.5, 0.5);
vec3 LightPos = vec3 (3.0);

bool intersectBox(vec3 ori, vec3 dir, vec3 boxMin, vec3 boxMax, out float t0, out float t1)
{
	vec3 invDir = 1.0 / dir;
	vec3 tBot = invDir * (boxMin.xyz - ori);
	vec3 tTop = invDir * (boxMax.xyz - ori);

	vec3 tMin = min(tTop, tBot);
	vec3 tMax = max(tTop, tBot);

	vec2 temp = max(tMin.xx, tMin.yz);
	float tMinMax = max(temp.x, temp.y);
	temp = min(tMax.xx, tMax.yz);
	float tMaxMin = min(temp.x, temp.y);

	bool hit;
	if((tMinMax > tMaxMin))
		hit = false;
	else
		hit = true;

	t0 = tMinMax;
	t1 = tMaxMin;

	return hit;
}

vec3 sampleGrad(sampler3D sampler, vec3 coord)
{
	const int offset = 1;
	float dx = textureOffset(sampler, coord, ivec3(offset, 0, 0)).r - textureOffset(sampler, coord, ivec3(-offset, 0, 0)).r;
	float dy = textureOffset(sampler, coord, ivec3(0, offset, 0)).r - textureOffset(sampler, coord, ivec3(0, -offset, 0)).r;
	float dz = textureOffset(sampler, coord, ivec3(0, 0, offset)).r - textureOffset(sampler, coord, ivec3(0, 0, -offset)).r;
	return vec3(dx, dy, dz);
}

//get texture coordinates
vec3 maptotex(vec3 min, vec3 max, vec3 pos)
{
    return (pos - min)/(max - min);
}

void main(void)
{
    vec3 dir = normalize(CameraDir);
    vec3 pos = CameraPos;

    vec3 boxMin = -volumeScale;
    vec3 boxMax = volumeScale;
    vec3 boxDim = boxMax - boxMin;

    float t0, t1;
    bool hit = intersectBox(pos, dir, boxMin, boxMax, t0, t1);
    if(!hit)
        discard;

    t0 = max(t0, 0.0);
    if(t1 <= t0)
        discard;
    if (t0 < 0.0) t0 = 0.0;

    //ray start/end
    vec3 ray_end = pos + dir * t1;
    pos += dir * t0;

    float dist = distance(pos, ray_end);
    float step = 0.001;

    float minx = min(pos.x, ray_end.x);
    float maxx = max(pos.x, ray_end.x);
    float miny = min(pos.y, ray_end.y);
    float maxy = max(pos.y, ray_end.y);
    float minz = min(pos.z, ray_end.z);
    float maxz = max(pos.z, ray_end.z);

    //color/lighting params
    vec3 c;
    vec3 color = vec3(0.0);
    float alpha = 0.0;

    //slicing (hook this up to UI eventually)
    float slice_x_min = -5.0;
    float slice_x_max = 5.0;
    float slice_y_min = 0.0;
    float slice_y_max = 0.0;
    float slice_z_min = 0.0;
    float slice_z_max = 0.0;

    vec3 slice_min = vec3(slice_x_min, slice_y_min, slice_z_min);
    vec3 slice_max = vec3(slice_x_max, slice_y_max, slice_z_max);

    //raycasting
    while (pos.x >= minx && pos.x <= maxx && pos.y >= miny && pos.y <= maxy
        && pos.z >= minz && pos.z <= maxz && alpha <= 1.0 && color.r < 1.0 &&
        color.g < 1.0 && color.b < 1.0) {

        //sampling texture

        /* normal raycasting:
        vec3 tex = maptotex(boxMin, boxMax, pos);
        vec4 data = texture(volumeTex, tex);
        vec4 rgba_sample = texture(transferFuncTex, data.xy);
        */

        //preintegration:
        vec3 tex_s = maptotex(boxMin, boxMax, pos);
        vec3 tex_t = maptotex(boxMin, boxMax, pos + dir*step);

        vec4 svec = texture(volumeTex, tex_s);
        vec4 tvec = texture(volumeTex, tex_t);
        float s = svec.x;
        float t = tvec.x;
        vec4 rgba_sample = texture(preintTex, vec2(s, t));

        //stuff for lighting

        vec3 lightdir = normalize(vec3 (LightPos - pos));

        vec3 norm = normalize(sampleGrad(volumeTex, tex_s));

        vec3 eyedir = normalize(vec3 (CameraPos - pos));

        vec3 refdir = reflect(-lightdir, norm);

        //ambient:
        c = color * 0.2;

        //diffuse:
        c += color * 1.5 * max(0.0, dot(norm, lightdir));

        //specular:
        c += LightColor * 0.5 * pow(max(0.0, dot(refdir, eyedir)), 30);

        //accumulate color & opacity

        /*
        float alpha_sample = (1.0 - pow(1.0 - rgba_sample.a, step/0.01));

        if (pos.x >= slice_max.x || pos.x <= slice_min.x) //slicing
        {
            color.rgb += 0;
        }
        else //normal accumulation
        {
            color.rgb += (1.0 - alpha_sample) * (rgba_sample.rgb) * alpha_sample;
            color += c * alpha_sample;
        }

        alpha += (1.0 - alpha_sample) * alpha_sample;
        */

        //preintegration accumulation:
        color += (1.0 - rgba_sample.w) * rgba_sample.xyz;
        alpha += (1.0 - rgba_sample.w) * rgba_sample.w;

        pos += dir*step;
    }

    //BOUNDING BOX
    if (bbtest == true && ((pos.x > 0.57 && pos.z > 0.57)
            ||
            (pos.x > 0.57 && pos.z < -0.57)
            ||
            (pos.x > 0.57 && pos.y < -0.57)
            ||
            (pos.x > 0.57 && pos.y > 0.57)
            ||
            (pos.x < -0.57 && pos.y < -0.57)
            ||
            (pos.x < -0.57 && pos.y > 0.57)
            ||
            (pos.x < -0.57 && pos.z < -0.57)
            ||
             (pos.x < -0.57 && pos.z > 0.57)
            ||
            (pos.y < -0.57 && pos.z > 0.57)
            ||
             (pos.y < -0.57 && pos.z < -0.57)
            ||
            (pos.y > 0.57 && pos.z < -0.57)
            ||
            (pos.y > 0.57 && pos.z > 0.57)))
    {
         color = vec3(0.0, .75, 1.0);
         alpha = 1.0;
    }

    FragColor = vec4 (color.rgb, alpha);
}
