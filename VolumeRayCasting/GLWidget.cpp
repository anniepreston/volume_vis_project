#include "GLWidget.h"
#include "Window.h"
#include <string>
#include <QDebug>
#include <fstream>
#include <vector>
#include <string>
#include <stdio.h>
#include <sstream>
#include <math.h>

GLWidget::GLWidget(const QGLFormat &format, QWidget *parent) :
    QGLWidget(format, parent)
{
    setFocusPolicy(Qt::ClickFocus);
    setMinimumSize(512, 512);

    //signal/slots:
    connect(this, SIGNAL(fileFinal(QString)), this, SLOT(setFile(QString)));

    connect(this, SIGNAL(tfFinal(QString)), this, SLOT(setTF(QString)));

    connect(this, SIGNAL(bgFinal(QString)), this, SLOT(setBG(QString)));

    connect(this, SIGNAL(bbFinal(QString)), this, SLOT(setBB(QString)));

    connect(this, SIGNAL(customTFFinal(bool)), this, SLOT(setCustomTF(bool)));
}

void GLWidget::initializeGL()
{
    //user selected background color
    if (bg_name == "")
        bgcolor = Vec4f(0.0, 0.0, 0.0, 0.0);
    else if (bg_name == "white")
        bgcolor = Vec4f(1.0, 1.0, 1.0, 0.0);
    else if (bg_name == "gray")
        bgcolor = Vec4f(0.7, 0.7, 0.7, 0.0);
    else if (bg_name == "black")
        bgcolor = Vec4f(0.0, 0.0, 0.0, 0.0);

    //bounding box
    if (BoundingBox == "")
        bbtest = false;
    else if (BoundingBox == "true")
        bbtest = true;
    else if (BoundingBox == "false")
        bbtest = false;

    glClearColor(bgcolor.x, bgcolor.y, bgcolor.z, bgcolor.w);

    glEnable(GL_BLEND);                 //blend computed fragment colors w/ color buffer vals
    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_FRONT);
    glDisable(GL_DEPTH_TEST);
    glColorMask(true, true, true, true); // r, g, b, a can be written into frame buffer
    glDepthMask(true);                   //depth buffer enabled for writing

    // initialize transformation
    rotation = Mat3f::identity();
    translation = Vec3f(0.0f, 0.0f, -2.0f);

    // load sample data volume
    volumeDim.x = 256;
    volumeDim.y = 256;
    volumeDim.z = 256;
    int volumeSize = volumeDim.x * volumeDim.y * volumeDim.z;
    std::vector<unsigned char> volumeData;
    volumeData.resize(volumeSize);

    Vec3f volumeScale = Vec3f(volumeDim);
    volumeScale = volumeScale / volumeScale.norm();

    //default dataset

    if (filename == "")
        filename = "skull.raw";

    std::ifstream ifs(filename, std::ios::binary);

    if(!ifs.is_open())
    {
        qDebug() << "Failed to open sample volume!";
        close();
    }

    ifs.read(reinterpret_cast<char *>(&volumeData.front()), volumeSize * sizeof(unsigned char));
    ifs.close();

    //create kernel for ambient occlusion
    /*
    std::vector<Vec3f> kernel;
    int kSize = 100;
    for (int i = 0; i < kSize; ++i) {
        std::vector<Vec3f> tempkernel;
        tempkernel = Vec3f(
        random (-1.0f, 1.0f),
        random (-1.0f, 1.0f),
        random (0.0f, 1.0f));
        tempkernel = tempkernel.norm(); //normalize
        tempkernel *= random(0.0f, 1.0f); //randomly distribute points within hemisphere
        kernel.push_back(tempkernel);
    }*/

    // prepare transfer function data
    std::vector<Vec4f> transferFuncData;

    //default
    if (tf_file == "")
        tf_file = "tf_skull.txt";

    std::ifstream transferfunc(tf_file);

    for(std::string line; std::getline(transferfunc, line); )
    {
        std::istringstream in(line);

        float r, g, b, a;
        in >> r >> g >> b >> a;
        transferFuncData.push_back(Vec4f(r, g, b, a));
    }

    transferfunc.close();

    // create OpenGL textures
    volumeTex = GLTexture::create();
    volumeTex->updateTexImage3D(GL_R8, volumeDim, GL_RED, GL_UNSIGNED_BYTE, &volumeData.front());

    transferFuncTex = GLTexture::create();
    transferFuncTex->updateTexImage2D(GL_RGBA32F, Vec2i(transferFuncData.size(), 1), GL_RGBA, GL_FLOAT, &transferFuncData.front());

    //preintegration data & 2D texture

    float TFsize = 8;
    float d = 0.01; //sample distance
    std::vector<std::vector<Vec4f>> preint(4, std::vector<Vec4f>(4)); //define this differently?

    for (int i = 0; i < TFsize; i++)
    {
        float s = i;
        Vec3f color;
        float alpha;

        //tau approx
        //float tau_s = 1.0 - transferFuncData[s].w;
        float tau_s = 50 * (sqrt(1.0 - transferFuncData[s].w) + 1.0);

        Vec3f c_s = Vec3f (transferFuncData[s].x, transferFuncData[s].y, transferFuncData[s].z);

        //tau approx
        //float tau_0 = transferFuncData[0].w;
        float tau_0 = 50 * (sqrt(1.0 - transferFuncData[0].w) + 1.0);

        Vec3f c_0 = Vec3f (transferFuncData[0].x, transferFuncData[0].y, transferFuncData[0].z);
        float T_s = (s/2.0) * (tau_s + tau_0);
        Vec3f K_s;
        K_s.x = (s/2.0) * (tau_s * c_s.x - tau_0 * c_0.x);
        K_s.y = (s/2.0) * (tau_s * c_s.y - tau_0 * c_0.y);
        K_s.z = (s/2.0) * (tau_s * c_s.z - tau_0 * c_0.z);

        std::vector<Vec4f> PI_row;

        for (int j = 0; j < TFsize; j++)
        {
            float t = j;
            //tau approximation
            float tau_t = 50 * (sqrt(1.0 - transferFuncData[t].w) + 1.0);

            Vec3f c_t = Vec3f (transferFuncData[t].x, transferFuncData[t].y, transferFuncData[t].z);
            float T_t = (t/2.0) * (tau_t + tau_0);
            Vec3f K_t;
            K_t.x = (t/2.0) * (tau_t * c_t.x - tau_0 * c_0.x);
            K_t.y = (t/2.0) * (tau_t * c_t.y - tau_0 * c_0.y);
            K_t.z = (t/2.0) * (tau_t * c_t.z - tau_0 * c_0.z);

            //s = t case (find a way to average?):
            if (s == t)
            {
                alpha = 1 - exp((-d) * (T_s - T_t));
                color.x = (d) * (K_s.x - K_t.x);
                color.y = (d) * (K_s.y - K_t.y);
                color.z = (d) * (K_s.z - K_t.z);
            }

            else
            {
                alpha = 1 - exp((-d/(t - s)) * (T_s - T_t));
                color.x = (d/(t - s)) * (K_s.x - K_t.x);
                color.y = (d/(t - s)) * (K_s.y - K_t.y);
                color.z = (d/(t - s)) * (K_s.z - K_t.z);
            }

            PI_row.push_back(Vec4f(fabs(color.x), fabs(color.y), fabs(color.z), fabs(alpha + 0.45)));

            qDebug() << color.x << color.y << color.z << alpha;

        }
        preint.push_back(PI_row);
    }

    preintTex = GLTexture::create();
    preintTex->updateTexImage2D(GL_RGBA32F, Vec2i(TFsize, TFsize), GL_RGBA, GL_FLOAT, &preint.front());

    // create full screen rectangle for volume ray-casting
    std::vector<Vec2f> rectVertices;
    rectVertices.push_back(Vec2f(0.0f, 0.0f));
    rectVertices.push_back(Vec2f(0.0f, 1.0f));
    rectVertices.push_back(Vec2f(1.0f, 1.0f));
    rectVertices.push_back(Vec2f(1.0f, 0.0f));
    rectVertexBuffer = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    rectVertexBuffer->update(rectVertices.size() * sizeof(Vec2f), &rectVertices.front(), GL_STATIC_DRAW);
    rectVertexArray = GLVertexArray::create();

    // create OpenGL shaders
    volumeRayCastingProgram = GLShaderProgram::create("VolumeRayCasting.vert", "VolumeRayCasting.frag");
    if(volumeRayCastingProgram == NULL)
        close();

    // assign vertex buffer to the shader attribute input called "Vertex"
    volumeRayCastingProgram->setVertexAttribute("Vertex", rectVertexArray, rectVertexBuffer, 2, GL_FLOAT, false);
}

void GLWidget::resizeGL(int w, int h)
{
    glViewport(0, 0, w, h);
    projectionMatrix = Mat4f::perspective(Math::pi<float>() * 0.25f, float(w) / float(h), 0.01f, 10.0f);
}

void GLWidget::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    Vec3f volumeScale = Vec3f(volumeDim);
    volumeScale = volumeScale / volumeScale.norm();

    modelViewMatrix = Mat4f(rotation, translation);
    Mat4f transform = Mat4f::ortho(0, 1, 0, 1, -1, 1);
    Mat4f invModelView = modelViewMatrix.inverse();
    volumeRayCastingProgram->setUniform("volumeScale", volumeScale);
    volumeRayCastingProgram->setUniform("transform", transform);
    volumeRayCastingProgram->setUniform("invModelView", invModelView);
    volumeRayCastingProgram->setUniform("aspect", float(width()) / float(height()));
    volumeRayCastingProgram->setUniform("bbtest", bbtest);
    volumeRayCastingProgram->setUniform("cotFOV", projectionMatrix.elem[1][1]);
    volumeRayCastingProgram->setUniform("kernel", kernel);
    volumeRayCastingProgram->setTexture("volumeTex", volumeTex);
    volumeRayCastingProgram->setTexture("transferFuncTex", transferFuncTex);
    volumeRayCastingProgram->setTexture("preintTex", preintTex);
    volumeRayCastingProgram->begin();
    rectVertexArray->drawArrays(GL_TRIANGLE_FAN, 4);
    volumeRayCastingProgram->end();
}

void GLWidget::mouseMoveEvent(QMouseEvent *event)
{
    QPointF pos = event->localPos();
    float dx = pos.x() - mousePos.x();
    float dy = pos.y() - mousePos.y();
    mousePos = pos;

    Qt::MouseButtons mouseButtons = event->buttons();

    if(mouseButtons & Qt::LeftButton)
    {
        rotation = Mat3f::fromAxisAngle(Vec3f::unitY(), dx * 0.005f) * rotation;
        rotation = Mat3f::fromAxisAngle(Vec3f::unitX(), dy * 0.005f) * rotation;
        rotation.orthonormalize();
    }

    if(mouseButtons & Qt::RightButton)
    {
        translation.z += dy * 0.005f;
        translation.z = clamp(translation.z, -9.0f, 1.0f);
    }

    if(mouseButtons & Qt::MidButton)
    {
        float scale = -std::min(translation.z, 0.0f) * 0.001f + 0.000025f;
        translation.x += dx * scale;
        translation.y -= dy * scale;
    }

    updateGL();
}

void GLWidget::mousePressEvent(QMouseEvent *event)
{
    mousePos = event->localPos();
}

void GLWidget::mouseReleaseEvent(QMouseEvent *event)
{
    mousePos = event->localPos();
}

void GLWidget::wheelEvent(QWheelEvent * event)
{
    QPoint numPixels = event->pixelDelta();
    QPoint numDegrees = event->angleDelta() / 8;
    float dx = 0.0f, dy = 0.0f;
    if(!numPixels.isNull())
    {
        dx = numPixels.x();
        dy = numPixels.y();
    }
    else if(!numDegrees.isNull())
    {
        dx = numDegrees.x() / 15;
        dy = numDegrees.y() / 15;
        dy = numDegrees.y() * -1;
    }

    Qt::KeyboardModifiers km = QApplication::keyboardModifiers();

    if(km & Qt::ControlModifier)
    {
        rotation = Mat3f::fromAxisAngle(Vec3f::unitY(), dx * 0.005f) * rotation;
        rotation = Mat3f::fromAxisAngle(Vec3f::unitX(), dy * 0.005f) * rotation;
    }
    else if(km & Qt::AltModifier)
    {
        float scale = -std::min(translation.z, 0.0f) * 0.001f + 0.000025f;
        translation.x += dx * scale;
        translation.y -= dy * scale;
    }
    else if(km & Qt::ShiftModifier)
    {
        translation.z += dy * 0.005f;
        translation.z = clamp(translation.z, -9.0f, 1.0f);
    }

    updateGL();
}

//change dataset to input choice
void GLWidget::setFile(QString file)
{
    filename = file.toStdString();

    initializeGL();
    updateGL();
}

void GLWidget::updateFile(QString file)
{
    emit fileFinal(file);
}

//same, for transfer function
void GLWidget::setTF(QString tf)
{
    tf_file = tf.toStdString();

    initializeGL();
    updateGL();
}

void GLWidget::updateTF(QString tf)
{
    emit tfFinal(tf);
}

//change background color
void GLWidget::setBG(QString bg)
{
    bg_name = bg.toStdString();

    initializeGL();
    updateGL();
}

void GLWidget::updateBG(QString bg)
{
    emit bgFinal(bg);
}

//bounding box
void GLWidget::setBB(QString bb)
{
    BoundingBox = bb.toStdString();
    qDebug() << bb;

    initializeGL();
    updateGL();
}

void GLWidget::updateBB(QString bb)
{
    emit bbFinal(bb);
}

//custom transfer function editor
void GLWidget::setCustomTF(bool)
{
    tf_file = "tf_custom.txt";

    initializeGL();
    updateGL();
}

void GLWidget::updateCustomTF(bool)
{
    emit customTFFinal(true);
}
