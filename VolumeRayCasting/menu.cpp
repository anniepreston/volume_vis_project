


#include <QtGui>
#include <QGLWidget>

#include "GLWidget.h"
#include "SliderWidget.h"
#include "Window.h"

SliderWidget::SliderWidget(QWidget *parent) :
    QWidget(parent)
{
    slider = createSlider();

    delta = 1;

    connect(slider, SIGNAL(valueChanged(int)), this, SLOT(updateDelta(int)));
    connect(this, SIGNAL(deltaChanged(int)), slider, SLOT(setValue(int)));

    QVBoxLayout *mainLayout = new QVBoxLayout(this);
    mainLayout->addWidget(slider);
    setLayout(mainLayout);

    slider->setValue(30);

    setWindowTitle(tr("step size"));
    slider->resize(200, 200);
}

QSlider *SliderWidget::createSlider()
{
    QSlider *slider = new QSlider(Qt::Horizontal);
    slider->setRange(1, 1000);
    slider->setSingleStep(1);
    return slider;
}

void
SliderWidget::updateDelta(int value)
{
    emit deltaChanged(value);
}

void Window::createActions()
{
    bonsaiAct = new QAction(tr("&bonsai"), glWidget);
    connect(bonsaiAct, SIGNAL(triggered()), glWidget, SLOT(setBonsai()));

    engineAct = new QAction(tr("&engine"), glWidget);
    connect(engineAct, SIGNAL(triggered()), glWidget, SLOT(setEngine()));

    skullAct = new QAction(tr("&skull"), glWidget);
    connect(skullAct, SIGNAL(triggered()), glWidget, SLOT(setSkull()));
}

void Window::createMenus()
{
    datasetMenu = menuBar()->addMenu(tr("&dataset"));
    datasetMenu->addAction(bonsaiAct);
    datasetMenu->addAction(engineAct);
    datasetMenu->addAction(skullAct);
}

