#ifndef GL_WIDGET_H
#define GL_WIDGET_H

#include <QGLWidget>
#include "GLArrayBuffer.h"
#include "GLShaderProgram.h"
#include "GLTexture.h"
#include "GLVertexArray.h"
#include "Mat.h"
#include "Vec.h"
#include <QApplication>
#include <string>

class GLWidget : public QGLWidget
{
    Q_OBJECT

public:
    GLWidget(const QGLFormat &format = QGLFormat::defaultFormat(), QWidget *parent = 0);
    virtual void initializeGL();

public slots:
    void setFile(QString file);
    void setTF(QString tf);
    void setBG(QString bg);
    void setBB(QString bb);
    void setCustomTF(bool);

    void updateFile(QString file);
    void updateTF(QString tf);
    void updateBG(QString bg);
    void updateBB(QString bb);
    void updateCustomTF(bool);

signals:
    void fileFinal(QString file);
    void tfFinal(QString tf);
    void bgFinal(QString bg);
    void bbFinal(QString bb);
    void customTFFinal(bool);

protected:
    //virtual void initializeGL();
    virtual void resizeGL(int w, int h);
    virtual void paintGL();
    virtual void mouseMoveEvent(QMouseEvent *event);
    virtual void mousePressEvent(QMouseEvent *event);
    virtual void mouseReleaseEvent(QMouseEvent *event);
    virtual void wheelEvent(QWheelEvent *event);

protected:
    QPointF mousePos;
    Mat3f rotation;
    Vec3f translation;
    Mat4f modelViewMatrix;
    Mat4f projectionMatrix;

    Vec3i volumeDim;
    Vec3f kernel;
    GLTexture::Ptr volumeTex;
    GLTexture::Ptr transferFuncTex;
    GLTexture::Ptr preintTex;
    GLArrayBuffer::Ptr rectVertexBuffer;
    GLArrayBuffer::Ptr rectIndexBuffer;
    GLVertexArray::Ptr rectVertexArray;
    GLShaderProgram::Ptr volumeRayCastingProgram;

private:
    std::string BoundingBox;
    bool bbtest;

    std::string filename;
    std::string tf_file;

    Vec4f bgcolor;
    std::string bg_name;

};

#endif // GLWIDGET_H
