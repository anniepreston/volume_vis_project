#ifndef TFWIDGET_H
#define TFWIDGET_H

#include <QtGui>
#include <QWidget>
#include <QGLWidget>

#include "GLWidget.h"

class HoverPoints;

class ShadeWidget : public QWidget
{
     Q_OBJECT

public:
    enum ShadeType
    {
        RGBShade,
        AShade
    };

     ShadeWidget(ShadeType type, QWidget *parent = 0);

     void setGradientStops(const QGradientStops &stops);

     void paintEvent(QPaintEvent *e);

     QSize sizeHint() const { return QSize(300, 100); }
     QPolygonF points() const;

     HoverPoints *hoverPoints() const { return m_hoverPoints; }

     uint colorAt(int x);

 signals:
     void colorsChanged();

 private:
     void generateShade();

     ShadeType m_shade_type;
     QImage m_shade;
     HoverPoints *m_hoverPoints;
     QLinearGradient m_alpha_gradient;
};

class GradientEditor : public QWidget
{
     Q_OBJECT

 public:
     GradientEditor(QWidget *parent = 0);

     void setGradientStops(const QGradientStops &stops);

 public slots:
     void pointsUpdated();


signals:
    void gradientStopsChanged(const QGradientStops &stops);

 private:
     ShadeWidget *m_rgb_shade;
     ShadeWidget *m_alpha_shade;

};

class GradientWidget : public QWidget
{
     Q_OBJECT
public:
     explicit GradientWidget(QWidget *parent = 0);

public slots:
     /*
     void setDefault1() { setDefault(1); }
     void setDefault2() { setDefault(2); }
     void setDefault3() { setDefault(3); }
     void setDefault4() { setDefault(4); }
     */


private:
     void setDefault(int i);

     GradientEditor *rgba_editor;

     GradientWidget *tfWidget;

     GLWidget* glWidget;

};

#endif // TFWIDGET_H
