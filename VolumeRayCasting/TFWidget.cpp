#include <QtGui>
#include <QGLWidget>
#include <QWidget>
#include <QFile>

#include <sstream>
#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>

#include <QTextStream>
#include <QFile>

#include "GLWidget.h"
#include "TFWidget.h"
#include "Window.h"

#include "hoverpoints.h"

ShadeWidget::ShadeWidget(ShadeType type, QWidget *parent) :
    QWidget(parent), m_shade_type(type), m_alpha_gradient(QLinearGradient(0, 0, 0, 0))
{
    QPolygonF points;
    points << QPointF(0, sizeHint().height())
           << QPointF(sizeHint().width(), 0);

    m_hoverPoints = new HoverPoints(this, HoverPoints::CircleShape);
    m_hoverPoints->setPoints(points);
    m_hoverPoints->setPointLock(0, HoverPoints::LockToLeft);
    m_hoverPoints->setPointLock(1, HoverPoints::LockToRight);
    m_hoverPoints->setSortType(HoverPoints::XSort);

    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);

    connect(m_hoverPoints, SIGNAL(pointsChanged(QPolygonF)), this, SIGNAL(colorsChanged()));
}

QPolygonF ShadeWidget::points() const
{
    return m_hoverPoints->points();
}

uint ShadeWidget::colorAt(int x)
{
    generateShade();

    QPolygonF pts = m_hoverPoints->points();
    for (int i = 1; i <pts.size(); i++)
    {
        if (pts.at(i-1).x() <= x && pts.at(i).x() >= x)
        {
            QLineF l(pts.at(i-1), pts.at(i));
            l.setLength(l.length() * ((x - l.x1()) / l.dx()));
            return m_shade.pixel(qRound(qMin(l.x2(), (qreal(m_shade.width() - 1)))),
                                 qRound(qMin(l.y2(), qreal(m_shade.height() - 1))));
        }
    }
    return 0;
}

void ShadeWidget::setGradientStops(const QGradientStops &stops)
{

    if (m_shade_type == AShade)
    {
        m_alpha_gradient = QLinearGradient(0, 0, width(), 0);

        for (int i = 0; i < stops.size(); i++)
        {
            QColor c = stops.at(i).second;
            m_alpha_gradient.setColorAt(stops.at(i).first, QColor(c.red(), c.green(), c.blue()));
        }

        m_shade = QImage();
        generateShade();
        update();
    }

}

void ShadeWidget::paintEvent(QPaintEvent *)
{
    generateShade();

    QPainter p(this);
    p.drawImage(0, 0, m_shade);

    p.setPen(QColor(146, 146, 146));
    p.drawRect(0, 0, width() - 1, height() - 1);
}

void ShadeWidget::generateShade()
{
    if (m_shade.isNull() || m_shade.size() != size())
    {
        if (m_shade_type == AShade)
        {
            m_shade = QImage(size(), QImage::Format_ARGB32_Premultiplied);
            m_shade.fill(0);

            QPainter p(&m_shade);
            p.fillRect(rect(), m_alpha_gradient);

            p.setCompositionMode(QPainter::CompositionMode_DestinationIn);
            QLinearGradient fade(0, 0, 0, height());
            fade.setColorAt(0, QColor(0, 0, 0, 255));
            fade.setColorAt(1, QColor(0, 0, 0, 0));
            p.fillRect(rect(), fade);
        }

        else
        {
            m_shade = QImage(size(), QImage::Format_RGB32);
            QLinearGradient shade(0, 0, 0, height());
            shade.setColorAt(1, Qt::green);
            shade.setColorAt(0.9, Qt::darkGreen);
            shade.setColorAt(0.8, Qt::darkBlue);
            shade.setColorAt(0.7, Qt::blue);
            shade.setColorAt(0.6, Qt::cyan);
            shade.setColorAt(0.5, Qt::magenta); //gradient through all colors
            shade.setColorAt(0.4, Qt::red);
            shade.setColorAt(0.3, Qt::darkYellow);
            shade.setColorAt(0.2, Qt::yellow);
            shade.setColorAt(0.1, Qt::white);
            shade.setColorAt(0, Qt::darkGray);

            QPainter p(&m_shade);
            p.fillRect(rect(), shade);
        }
    }
}

GradientEditor::GradientEditor(QWidget *parent) :
    QWidget(parent)
{
    QVBoxLayout *vbox = new QVBoxLayout(this);
    vbox->setSpacing(1);
    vbox->setMargin(1);

    m_rgb_shade = new ShadeWidget(ShadeWidget::RGBShade, this);
    m_alpha_shade = new ShadeWidget(ShadeWidget::AShade, this);

    vbox->addWidget(m_rgb_shade);
    vbox->addWidget(m_alpha_shade);

    connect(m_rgb_shade, SIGNAL(colorsChanged()), this, SLOT(pointsUpdated()));
    connect(m_alpha_shade, SIGNAL(colorsChanged()), this, SLOT(pointsUpdated()));
}

inline static bool x_less_than(const QPointF &p1, const QPointF &p2)
{
    return p1.x() < p2.x();
}

void GradientEditor::pointsUpdated()
{
    QFile custom_tf("/Users/anniepreston/repos/volume_vis_project/VolumeRayCasting/tf_custom.txt"); //file for writing TF colors
    custom_tf.open(QIODevice::ReadWrite | QIODevice::Text);
    QTextStream out(&custom_tf);

    qreal w = m_alpha_shade->width();

    QGradientStops stops;

    QPolygonF points;

    points += m_rgb_shade->points();
    points += m_alpha_shade->points();

    qSort(points.begin(), points.end(), x_less_than);

    //std::vector<Vec4f> customTransferFuncData;

    for (int i = 0; i < points.size(); i++)
    {
        qreal x = int(points.at(i).x());
        if (i+1 < points.size() && x == points.at(i+1).x())
        {
            continue;
        }

        QColor color((0x00ff0000 & m_rgb_shade->colorAt(int(x))) >> 16,
                     (0x0000ff00 & m_rgb_shade->colorAt(int(x))) >> 8,
                     (0x000000ff & m_rgb_shade->colorAt(int(x))),
                     (0xff000000 & m_alpha_shade->colorAt(int(x))) >> 24);

        //write QColors to file
        float r = color.red()/255.;
        float g = color.green()/255.;
        float b = color.blue()/255.;
        float a = color.alpha()/255.;
        out << r << " " << g << " " << b << " " << a << "\n";

        if (x / w > 1)
        {
            return;
        }

       stops << QGradientStop(x / w, color);
    }

    m_alpha_shade->setGradientStops(stops);

    custom_tf.close();

    emit gradientStopsChanged(stops);

}

static void set_shade_points(const QPolygonF &points, ShadeWidget *shade)
{
    shade->hoverPoints()->setPoints(points);
    shade->hoverPoints()->setPointLock(0, HoverPoints::LockToLeft);
    shade->hoverPoints()->setPointLock(points.size() - 1, HoverPoints::LockToRight);
    shade->update();
}

void GradientEditor::setGradientStops(const QGradientStops &stops)
{
     QPolygonF pts_rgb, pts_alpha;

     qreal h_rgb = m_rgb_shade->height();
     qreal h_alpha = m_alpha_shade->height();

     for (int i=0; i<stops.size(); ++i) {
         qreal pos = stops.at(i).first;
         QRgb color = stops.at(i).second.rgba();
         pts_rgb << QPointF(pos * m_rgb_shade->width(), h_rgb - qBlue(color) * h_rgb / 255);
         pts_alpha << QPointF(pos * m_alpha_shade->width(), h_alpha - qAlpha(color) * h_alpha / 255);
     }

     set_shade_points(pts_rgb, m_rgb_shade);
     set_shade_points(pts_alpha, m_alpha_shade);

}

GradientWidget::GradientWidget(QWidget *parent):
    QWidget(parent)
{
     setWindowTitle(tr("Transfer Function"));

     //m_renderer = new GradientRenderer(this);

     QVBoxLayout *mainLayout = new QVBoxLayout(this);
     QGroupBox *mainGroup = new QGroupBox(this);

     rgba_editor = new GradientEditor(mainGroup);

     //add "commit TF" button
     QPushButton *commitTFButton = new QPushButton(mainGroup);
     commitTFButton->setText(tr("Update TF"));

     mainLayout->addWidget(rgba_editor);
     mainLayout->addWidget(commitTFButton);

     setLayout(mainLayout);

     //connect(rgba_editor, SIGNAL(gradientStopsChanged(QGradientStops)), glWidget, SLOT(updateCustomTF(QGradientStops)));

     //QTimer::singleShot(50, this, SLOT(setDefault1()));

     connect(commitTFButton, SIGNAL(clicked(bool)), glWidget, SLOT(updateCustomTF(bool)));
}
