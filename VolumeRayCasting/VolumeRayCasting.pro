#-------------------------------------------------
#
# Project created by QtCreator 2014-09-03T21:03:50
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets opengl

CONFIG += c++11

TARGET = VolumeRayCasting
TEMPLATE = app


SOURCES += main.cpp\
        GLWidget.cpp \
    GLShaderProgram.cpp \
    GLArrayBuffer.cpp \
    GLFramebuffer.cpp \
    GLTexture.cpp \
    GLVertexArray.cpp \
    Window.cpp \
    TFWidget.cpp \
    hoverpoints.cpp

HEADERS  += GLWidget.h \
    Mat.h \
    MathUtil.h \
    Vec.h \
    GLShaderProgram.h \
    GLArrayBuffer.h \
    GLConfig.h \
    GLFramebuffer.h \
    GLTexture.h \
    GLVertexArray.h \
    Window.h \
    TFWidget.h \
    hoverpoints.h

OTHER_FILES += \
    VolumeRayCasting.frag \
	VolumeRayCasting.vert \
    skull.raw \
    bonsai.raw \
    Engine.raw \
    tf_skull.txt \
    tf_bonsai.txt \
    tf_engine.txt \
    tf_custom.txt

Data.files += $$OTHER_FILES

macx: Data.path = Contents/MacOS
win32: Data.path = $$OUT_PWD

macx: QMAKE_BUNDLE_DATA += Data
win32: INSTALLS += Data

macx: QMAKE_MAC_SDK = macosx10.9

win32: LIBS += -L$$PWD/glew/ -lglew32
win32: INCLUDEPATH += $$PWD/glew
win32: DEPENDPATH += $$PWD/glew
win32: PRE_TARGETDEPS += $$PWD/glew/glew32.lib
win32: DESTDIR = $$OUT_PWD

FORMS +=
