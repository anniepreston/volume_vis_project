#include "GLWidget.h"
#include <QApplication>
#include "Window.h"
#include "TFWidget.h"

int main(int argc, char *argv[])
{
    QGLFormat glFormat = QGLFormat::defaultFormat();
    glFormat.setVersion(3, 3);
    glFormat.setProfile(QGLFormat::CoreProfile);
    glFormat.setSampleBuffers(true);              //multisample buffer support
    glFormat.setSamples(4);                       //number of samples per pixel
    QGLFormat::setDefaultFormat(glFormat);

    QApplication a(argc, argv);
    GLWidget* glWidget = new GLWidget();
    //ShadeWidget* tfWidget = new ShadeWidget();
    //GradientWidget tfWidget(0);
    GradientWidget* tfWidget = new GradientWidget(); //FIX parent/child stuff

    Window w(glWidget, tfWidget);
    w.show();

    return a.exec();
}
